Option Explicit On
Option Strict   On
Option Compare  Binary
Option Infer    On
Imports Stopwatch = System.Diagnostics.Stopwatch
Imports Console = System.Console

Public Module Main

    Dim sw As New Stopwatch()

    Public Sub Main()
        Try
            sw.Start()
            Dim _crosswordPuzzler As New CrosswordPuzzler()
            sw.Stop()

            ' do edit codes if you need

            CallCreatePuzzle(_crosswordPuzzler)

        Catch Ex As Exception
            Console.Error.WriteLine(ex)
        End Try
    End Sub

    Sub PrintDetails(wordList() As String)
        Dim tmp As String = ""
        Dim lens(12) As Integer
        Dim table(255, 12) As Integer
        Dim tc As Integer = 0
        For i As Integer = 0 To UBound(wordList)
            If i < 50 Then
                tmp += wordList(i) + ","
            End If
            lens(wordList(i).Length) += 1
            tc += wordList(i).Length
            For j As Integer = 0 To wordList(i).Length - 1
                Dim a As Integer = Asc(wordList(i)(j))
                table(a, j) += 1
            Next j
        Next i
        Console.Error.WriteLine(tmp)
        Dim tl As Integer = 0
        For i As Integer = 3 To 12
            tl += lens(i)
            Dim f As Double = CDbl(100 * lens(i)) / CDbl(wordList.Length)
            Dim g As Double = CDbl(100 * tl) / CDbl(wordList.Length)
            Console.Error.WriteLine("len: {0,2} ... {1,4} ... {2,5:F2}% ... {3,6:F2}%", i, lens(i), f, g)
        Next i
        tmp = ""
        For i As Integer = 0 To 7
            Dim e As Integer = 1 << i
            tmp += String.Format(" {0,6}", e)
        Next i
        Console.Error.WriteLine(tmp)
        tmp = ""
        For i As Integer = 0 To 7
            Dim e As Integer = 1 << i
            Dim p As Double = CDbl(e * 100) / CDbl(tc)
            tmp += String.Format(" {0,5:F2}%", p)
        Next i
        Console.Error.WriteLine(tmp)
        For i As Integer = Asc("A") To Asc("Z")
            tmp = String.Format("{0}:", Chr(i))
            Dim t As Integer = 0
            For j As Integer = 0 To 11
                t += table(i, j)
                tmp += String.Format(" {0,3}", table(i, j))
            next j
            tmp += String.Format(" {0,5}", t)
            Console.Error.WriteLine(tmp)
        Next i
    End Sub

    Function CallCreatePuzzle(_crosswordPuzzler As CrosswordPuzzler) As Integer
        Dim width As Integer = CInt(Console.ReadLine())
        Dim height As Integer = CInt(Console.ReadLine())
        Dim _dictionarySize As Integer = CInt(Console.ReadLine()) - 1
        Dim dictionary(_dictionarySize) As String
        For _idx As Integer = 0 To _dictionarySize
            dictionary(_idx) = Console.ReadLine()
        Next _idx

        PrintDetails(dictionary)

        sw.Start()
        Dim _result As String() = _crosswordPuzzler.createPuzzle(width, height, dictionary)
        sw.Stop()
        Console.Error.WriteLine("time: {0} ms", sw.ElapsedMilliseconds)

        Console.WriteLine(_result.Length)
        For Each _it As String In _result
            Console.WriteLine(_it)
        Next _it
        Console.Out.Flush()

        Return 0
    End Function
End Module