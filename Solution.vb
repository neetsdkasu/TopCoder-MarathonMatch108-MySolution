Option Explicit On
Option Strict   On
Option Compare  Binary
Option Infer    On

Imports System
Imports System.Collections.Generic
Imports Tp = System.Tuple(Of Integer, Integer)
Imports Solution = System.Tuple(Of Char()(), Integer)

Public Class CrosswordPuzzler

    Const B1 As Byte = 1

    ReadOnly Fib() As Integer = { 0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144 }

    Dim startTime As Integer, limitTime As Integer
    Dim rand As New Random(19831983)

    Dim height, width As Integer
    Dim offs4row(12) As Integer
    Dim offs4col(12) As Integer

    Public Function createPuzzle(width As Integer, height As Integer, wordList() As String) As String()
        startTime = Environment.TickCount
        limitTime = startTime + 9000
        Console.Error.WriteLine("{0}, {1}", limitTime, rand.Next(0, 100))
        Console.Error.WriteLine("w: {0}, h: {1}, n: {2}", width, height, wordList.Length)

        Me.height = height
        Me.width = width

        offs4row(3) = 1
        offs4col(3) = 1
        For i As Integer = 3 To 11
            offs4row(i + 1) = offs4row(i) * ((width + 1) \ (i + 1) + 1)
            offs4col(i + 1) = offs4col(i) * ((height + 1) \ (i + 1) + 1)
        Next i

        Dim wordsByLen(12) As List(Of String)
        For i As Integer = 3 To 12
            wordsByLen(i) = New List(Of String)()
        Next i

        For Each w As String In wordList
            wordsByLen(w.Length).Add(w)
        Next w

        Dim sol As Solution = GetRowDPSolution(wordsByLen, startTime + 4500)
        Dim tmp As Solution = GetColDPSolution(wordsByLen, startTime + 9050)
        If tmp.Item2 > sol.Item2 Then sol = tmp

        Dim ret(height - 1) As String
        For i As Integer = 0 To UBound(ret)
            ret(i) = New String(sol.Item1(i))
            Console.Error.WriteLine(ret(i))
        Next i
        createPuzzle = ret
    End Function

    Private Function NewField() As Char()()
        Dim f(height - 1)() As Char
        For i As Integer = 0 To UBound(f)
            ReDim f(i)(width - 1)
        Next i
        ClearField(f)
        NewField = f
    End Function

    Private Sub ClearField(f()() As Char)
        For Each g() As Char In f
            For j As Integer = 0 To UBound(g)
                g(j) = "-"c
            Next j
        Next g
    End Sub

    Private Sub Shuffle(Of T)(xs As List(Of T))
        For i As Integer = xs.Count To 1 Step -1
            Dim j As Integer = rand.Next(i)
            Dim x As T = xs(j)
            xs(j) = xs(i - 1)
            xs(i - 1) = x
        Next i
    End Sub

    Private Sub Shuffle(Of T)(xs() As T)
        For i As Integer = xs.Length To 1 Step -1
            Dim j As Integer = rand.Next(i)
            Dim x As T = xs(j)
            xs(j) = xs(i - 1)
            xs(i - 1) = x
        Next i
    End Sub

    Private Function GetRowDPSolution(wordsByLen() As List(Of String), stopTime As Integer) As Solution
        Dim baseScore As Integer = - (height * width)

        Dim counts(12) As Integer
        For i As Integer = 3 To 12
            counts(i) = wordsByLen(i).Count
        Next i

        Dim rows(height \ 2)() As Integer

        For row As Integer = 0 To height - 1 Step 2
            Dim dp(width + 1) As Tp
            dp(0) = New Tp(0, 0)
            For i As Integer = 3 To 12
                For j As Integer = 1 To Math.Min(counts(i), (width + 1) \ (i + 1))
                    For p As Integer = width - i To 0 Step -1
                        If dp(p) Is Nothing Then Continue For
                        If dp(p + i + 1) IsNot Nothing AndAlso dp(p).Item1 + Fib(i) <= dp(p + i + 1).Item1 Then
                            Continue For
                        End If
                        dp(p + i + 1) = New Tp(dp(p).Item1 + Fib(i), dp(p).Item2 + offs4row(i))
                    Next p
                Next j
            Next i
            Dim best As Tp = dp(0)
            For i As Integer = 1 To UBound(dp)
                If dp(i) Is Nothing Then Continue For
                If dp(i).Item1 > best.Item1 Then best = dp(i)
            Next i
            If best.Item2 = 0 Then Continue For
            Dim v As Integer = best.Item2
            Dim ys As New List(Of Integer)()
            Dim tw As Integer = 0
            For e As Integer = 12 To 3 Step -1
                Dim c As Integer = v \ offs4row(e)
                If c > 0 Then
                    v -= c * offs4row(e)
                    counts(e) -= c
                    tw += c * e
                    For i As Integer = 1 To c
                        ys.Add(e)
                    Next i
                End If
            Next e
            For i As Integer = 1 To width - tw - (ys.Count - 1)
                ys.Add(-1)
            Next i
            rows(row \ 2) = ys.ToArray()
            baseScore += best.Item1 + tw
        Next row

                Dim used(12)() As Boolean
        For i As Integer = 3 To 12
            Redim used(i)(wordsByLen(i).Count - 1)
        Next i

        Dim bestField()() As Char = NewField()
        Dim bestScore As Integer = -(height * width)

        Dim f()() As Char = NewField()

        For cycle As Integer = 1 To 2000000
            Dim time1 As Integer = Environment.TickCount
            If time1 > stopTime Then
                Console.Error.WriteLine("row-cycle: {0}", cycle)
                Exit For
            End If
            For i As Integer = 3 To 12
                counts(i) = wordsByLen(i).Count
            Next i

            Dim y As Integer = 0
            For Each ws() As Integer In rows
                If ws Is Nothing Then
                    y += 1
                    Continue For
                End If
                Dim x As Integer = 0
                For Each e As Integer In ws
                    If e < 0 Then
                        x += 1
                        Continue For
                    End If
                    counts(e) -= 1
                    used(e)(counts(e)) = True
                    Dim w As String = wordsByLen(e)(counts(e))
                    For dc As Integer = 0 To e - 1
                        f(y)(x + dc) = w(dc)
                    Next dc
                    x += e + 1
                Next e
                y += 2
            Next ws

            Dim score As Integer = baseScore

            For e As Integer = 12 To 3 Step -1
                Dim words As List(Of String) = wordsByLen(e)
                For i As Integer = 0 To counts(e) - 1
                    If used(e)(i) Then Continue For
                    Dim w As String = words(i)
                    For col As Integer = 0 To width - 1
                        For row As Integer = 0 To height - e
                            If row > 0 AndAlso f(row - 1)(col) <> "-"c Then
                                Continue For
                            End If
                            If row + e < height AndAlso f(row + e)(col) <> "-"c Then
                                Continue For
                            End If
                            Dim ok As Boolean = True
                            Dim emp As Integer = 0
                            For dr As Integer = 0 To e - 1
                                If f(row + dr)(col) = "-"c Then
                                    If col > 0 AndAlso f(row + dr)(col - 1) <> "-"c Then
                                        ok = False
                                        Exit For
                                    End If
                                    If col + 1 < width AndAlso f(row + dr)(col + 1) <> "-"c Then
                                        ok = False
                                        Exit For
                                    End If
                                    emp += 1
                                ElseIf f(row + dr)(col) <> w(dr) Then
                                    ok = False
                                    Exit For
                                End If
                            Next dr
                            If Not ok Then Continue For
                            used(e)(i) = True
                            For dr As Integer = 0 To e - 1
                                f(row + dr)(col) = w(dr)
                            Next dr
                            score += Fib(e) + emp
                            GoTo OutOfLoop
                        Next row
                    Next col
                OutOfLoop:
                Next i
            Next e

            If score > bestScore Then
                Dim tmp()() As Char = f
                f = bestField
                bestField = tmp
                bestScore = score
            End If

            For i As Integer = 3 To 12
                Array.Clear(used(i), 0, used(i).Length)
                Shuffle(wordsByLen(i))
            Next i
            ClearField(f)
            Shuffle(rows)
            For Each ws() As Integer In rows
                If ws IsNot Nothing Then
                    Shuffle(ws)
                End If
            Next ws

        Next cycle

        Console.Error.WriteLine("rowDP: {0}", bestScore)

        GetRowDPSolution = New Solution(bestField, bestScore)
    End Function

    Private Function GetColDPSolution(wordsByLen() As List(Of String), stopTime As Integer) As Solution
        Dim baseScore As Integer = - (height * width)

        Dim counts(12) As Integer
        For i As Integer = 3 To 12
            counts(i) = wordsByLen(i).Count
        Next i

        Dim cols(width \ 2)() As Integer

        For col As Integer = 0 To width - 1 Step 2
            Dim dp(height + 1) As Tp
            dp(0) = New Tp(0, 0)
            For i As Integer = 3 To 12
                For j As Integer = 1 To Math.Min(counts(i), (height + 1) \ (i + 1))
                    For p As Integer = height - i To 0 Step -1
                        If dp(p) Is Nothing Then Continue For
                        If dp(p + i + 1) IsNot Nothing AndAlso dp(p).Item1 + Fib(i) <= dp(p + i + 1).Item1 Then
                            Continue For
                        End If
                        dp(p + i + 1) = New Tp(dp(p).Item1 + Fib(i), dp(p).Item2 + offs4col(i))
                    Next p
                Next j
            Next i
            Dim best As Tp = dp(0)
            For i As Integer = 1 To UBound(dp)
                If dp(i) Is Nothing Then Continue For
                If dp(i).Item1 > best.Item1 Then best = dp(i)
            Next i
            If best.Item2 = 0 Then Continue For
            Dim v As Integer = best.Item2
            Dim xs As New List(Of Integer)()
            Dim th As Integer = 0
            For e As Integer = 12 To 3 Step -1
                Dim c As Integer = v \ offs4col(e)
                If c > 0 Then
                    v -= c * offs4col(e)
                    counts(e) -= c
                    th += c * e
                    For i As Integer = 1 To c
                        xs.Add(e)
                    Next i
                End If
            Next e
            For i As Integer = 1 To height - th - (xs.Count - 1)
                xs.Add(-1)
            Next i
            cols(col \ 2) = xs.ToArray()
            baseScore += best.Item1 + th
        Next col

        Dim used(12)() As Boolean
        For i As Integer = 3 To 12
            Redim used(i)(wordsByLen(i).Count - 1)
        Next i

        Dim bestField()() As Char = NewField()
        Dim bestScore As Integer = -(height * width)

        Dim f()() As Char = NewField()

        For cycle As Integer = 1 To 2000000
            Dim time1 As Integer = Environment.TickCount
            If time1 > stopTime Then
                Console.Error.WriteLine("col-cycle: {0}", cycle)
                Exit For
            End If
            For i As Integer = 3 To 12
                counts(i) = wordsByLen(i).Count
            Next i

            Dim x As Integer = 0
            For Each hs() As Integer In cols
                If hs Is Nothing Then
                    x += 1
                    Continue For
                End If
                Dim y As Integer = 0
                For Each e As Integer In hs
                    If e < 0 Then
                        y += 1
                        Continue For
                    End If
                    counts(e) -= 1
                    used(e)(counts(e)) = True
                    Dim w As String = wordsByLen(e)(counts(e))
                    For dr As Integer = 0 To e - 1
                        f(y + dr)(x) = w(dr)
                    Next dr
                    y += e + 1
                Next e
                x += 2
            Next hs

            Dim score As Integer = baseScore

            For e As Integer = 12 To 3 Step -1
                Dim words As List(Of String) = wordsByLen(e)
                For i As Integer = 0 To counts(e) - 1
                    If used(e)(i) Then Continue For
                    Dim w As String = words(i)
                    For row As Integer = 0 To height - 1
                        For col As Integer = 0 To width - e
                            If col > 0 AndAlso f(row)(col - 1) <> "-"c Then
                                Continue For
                            End If
                            If col + e < width AndAlso f(row)(col + e) <> "-"c Then
                                Continue For
                            End If
                            Dim ok As Boolean = True
                            Dim emp As Integer = 0
                            For dc As Integer = 0 To e - 1
                                If f(row)(col + dc) = "-"c Then
                                    If row > 0 AndAlso f(row - 1)(col + dc) <> "-"c Then
                                        ok = False
                                        Exit For
                                    End If
                                    If row + 1 < height AndAlso f(row + 1)(col + dc) <> "-"c Then
                                        ok = False
                                        Exit For
                                    End If
                                    emp += 1
                                ElseIf f(row)(col + dc) <> w(dc) Then
                                    ok = False
                                    Exit For
                                End If
                            Next dc
                            If Not ok Then Continue For
                            used(e)(i) = True
                            For dc As Integer = 0 To e - 1
                                f(row)(col + dc) = w(dc)
                            Next dc
                            score += Fib(e) + emp
                            GoTo OutOfLoop
                        Next col
                    Next row
                OutOfLoop:
                Next i
            Next e

            If score > bestScore Then
                Dim tmp()() As Char = f
                f = bestField
                bestField = tmp
                bestScore = score
            End If

            For i As Integer = 3 To 12
                Array.Clear(used(i), 0, used(i).Length)
                Shuffle(wordsByLen(i))
            Next i
            ClearField(f)
            Shuffle(cols)
            For Each hs() As Integer In cols
                If hs IsNot Nothing Then
                    Shuffle(hs)
                End If
            Next hs

        Next cycle

        Console.Error.WriteLine("colDP: {0}", bestScore)

        GetColDPSolution = New Solution(bestField, bestScore)
    End Function
End Class